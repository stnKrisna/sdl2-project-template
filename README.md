
# SDL2 + DearImGui + Zip Template

This is a simple project that uses SDL2 + DearImGui and CMake as the build generator. I have also included a lightweight header only zip library to handle basic asset archive. I used this [repo](https://github.com/atdean/sdl2-cpp-project-template) as a starting point. But, since I was not satisfied on the MacOS compilation, I created this.

This project will not assume that you have SDL2 installed in your system so you must add the SDL2 dependency into your project directory.

## Adding SDL Framework

**MacOS**
1. Download SDL framework ([as of writing](https://github.com/libsdl-org/SDL/releases/tag/release-2.28.4))
1. Copy the `SDL2.framework` to `./framework/macOS`

**Windows**
Untested
**Linux**
Untested

## Adding DearImGui

  

1. Download DearImGui (https://github.com/ocornut/imgui)
1. Extract the archive
1. Open this file: ``src/imgui/README.txt`` and follow the readme

  

If you don't want to include DearImGui, open this file `src/imgui/imgui_initializer.hpp` and uncomment this line:

```
// #define DISABLE_DEAR_IMGUI
```

## Adding Miniz

You don't need to do anything. The header is already included in the source.

Read more: https://github.com/tfussell/miniz-cpp/tree/master

## Compiling

  

On macOS, the source code will be compiled into an application bundle. The `SDL2.framework` will also be coppied into the bundle so that you are ready to distribute your application. You will need to, either zip your app bundle, or create a distribution image. However, you might still need to sign your application

  

1. Go to the `build` directory
1. Run `cmake ..`
1. Run `make`

## TO-DO
Use git submodules?