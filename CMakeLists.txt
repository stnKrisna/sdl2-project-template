set(PROJECT_NAME sdl2-cpp-project-template)

# https://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
# Fix behavior of CMAKE_CXX_STANDARD when targeting macOS.
if (POLICY CMP0025)
  cmake_policy(SET CMP0025 NEW)
endif ()

# https://trenki2.github.io/blog/2017/06/02/using-sdl2-with-cmake/
# Include the FindSDL2.cmake file in order to find SDL2 on all platforms
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake")

cmake_minimum_required(VERSION 3.15.0)
set(CMAKE_CXX_STANDARD 11)

project(${PROJECT_NAME})

# Can manually add sources as follows if desired:
# set(SOURCES src/main.cpp src/anotherfile.cpp)

# Use wildcard to include all source files under /src
# Can also use GLOB_RECURSE
file(GLOB SOURCES
  "src/*.cpp"
  "src/imgui/imgui_initializer.cpp"
  "src/imgui/*.cpp"
)

# set(SDL2_INCLUDE_DIRS /Library/Frameworks/SDL2.framework/Headers)
# set(SDL2_LIBRARIES /Library/Frameworks/SDL2/framework/SDL2)

# find_package(SDL2 REQUIRED)
include_directories(${SDL2_INCLUDE_DIRS} ${SDL2_IMAGE_INCLUDE_DIRS})

if (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
  # Setup framework path
  set(CMAKE_SKIP_BUILD_RPATH TRUE)

  set(APP_BUNDLE_CONTENTS_DIR "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.app/Contents")
  set(APP_BUNDLE_FRAMEWORKS_DIR "${APP_BUNDLE_CONTENTS_DIR}/Frameworks")

  set(SDL2_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/framework/macOS/SDL2.framework/Headers)
  set(SDL2_FRAMEWORK_PATH ${CMAKE_SOURCE_DIR}/framework/macOS/SDL2.framework)

  add_executable(${PROJECT_NAME} MACOSX_BUNDLE ${SOURCES})
else()
  add_executable(${PROJECT_NAME} ${SOURCES})
endif()

include_directories(${SDL2_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME} ${SDL2_FRAMEWORK_PATH})

# When on MacOS, compile as app bundle
# https://stackoverflow.com/a/65928709
if (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
  set_target_properties(${PROJECT_NAME} PROPERTIES
    BUNDLE True
    MACOSX_BUNDLE_GUI_IDENTIFIER my.domain.style.identifier.${PROJECT_NAME}
    MACOSX_BUNDLE_BUNDLE_NAME ${PROJECT_NAME}
    MACOSX_BUNDLE_BUNDLE_VERSION "0.1"
    MACOSX_BUNDLE_SHORT_VERSION_STRING "0.1"
    MACOSX_BUNDLE_INFO_PLIST ${CMAKE_SOURCE_DIR}/cmake/MacOSXBundleInfo.plist.in
  )

  file(COPY ${SDL2_FRAMEWORK_PATH} DESTINATION ${APP_BUNDLE_FRAMEWORKS_DIR})

  add_custom_command(TARGET ${PROJECT_NAME} 
    POST_BUILD COMMAND 
    install_name_tool -add_rpath "@executable_path/../Frameworks/"
    "${APP_BUNDLE_CONTENTS_DIR}/MacOS/${PROJECT_NAME}")
endif()
