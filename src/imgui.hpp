#pragma once

#include "imgui/imgui_initializer.hpp"

#ifndef DISABLE_DEAR_IMGUI
#include "imgui/imgui_stdlib.h"
#endif