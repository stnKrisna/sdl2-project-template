#include <SDL2/SDL.h>
#include <iostream>
#include "zip_file.hpp"
#include "imgui.hpp"

#ifndef MZ_VERSION
#define MZ_VERSION "N/A"
#endif

int main(int argc, const char* argv[]) {
    bool quit = false;
    SDL_Event event;

    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window* window = SDL_CreateWindow(
        "SDL2 Starter Project",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        640, 480, 0
    );
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);

    ImGui::InitAll(window, renderer);

    while (!quit)
    {
        SDL_Delay(10);
        ImGui::MakeFrame();
        
        while (SDL_PollEvent(&event)) {
            ImGui::ProcessEvent(&event);
            switch (event.type) {
                case SDL_QUIT:
                    quit = true;
                    break;
            }
        }

        SDL_SetRenderDrawColor(renderer, 242, 242, 242, 255);
        SDL_RenderClear(renderer);

#ifndef HIDE_LAUNCH_DEBUG
#ifndef DISABLE_DEAR_IMGUI
        ImGui::Begin("Build Info");
        ImGui::Text("SDL: %d.%d.%d", SDL_MAJOR_VERSION, SDL_MINOR_VERSION, SDL_PATCHLEVEL);
        ImGui::Text("DearImGui: %s", ImGui::GetVersion());
        ImGui::Text("Miniz: %s", MZ_VERSION);
        ImGui::End();
#endif
#endif

        ImGui::RenderFrame();
        SDL_RenderPresent(renderer);
    }

    ImGui::ShutdownAll();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
