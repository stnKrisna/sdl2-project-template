#pragma once

// #define DISABLE_DEAR_IMGUI

#ifndef DISABLE_DEAR_IMGUI
#include "imgui.h"
#include "imgui_impl_sdl2.h"
#else
// Need to include SDL2 because of the SDL refs
#include <SDL2/SDL.h>
#endif

namespace ImGui {
    /**
     * Perform ImGui initialization
    */
    void InitAll (SDL_Window * sdlWindow, SDL_Renderer * sdlRenderer);

    /**
     * Process SDL2 event to ImGui
    */
    void ProcessEvent (const SDL_Event *event);

    /**
     * Perform new frame routine
    */
    void MakeFrame ();

    /**
     * Perform ImGui frame render
    */
    void RenderFrame ();

    /**
     * Perform ImGui shutdown sequence
    */
    void ShutdownAll ();
}