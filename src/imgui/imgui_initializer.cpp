#include "imgui_initializer.hpp"

#ifndef DISABLE_DEAR_IMGUI
#include "imgui_impl_sdlrenderer2.h"
#endif

using namespace ImGui;

void ImGui::InitAll (SDL_Window * sdlWindow, SDL_Renderer * sdlRenderer) {
#ifndef DISABLE_DEAR_IMGUI
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    ImGui::StyleColorsDark();

    ImGui_ImplSDL2_InitForSDLRenderer(sdlWindow, sdlRenderer);
    ImGui_ImplSDLRenderer2_Init(sdlRenderer);
#endif
}

void ImGui::ProcessEvent (const SDL_Event *event) {
#ifndef DISABLE_DEAR_IMGUI
    ImGui_ImplSDL2_ProcessEvent(event);
#endif
}

void ImGui::MakeFrame () {
#ifndef DISABLE_DEAR_IMGUI
    ImGui_ImplSDLRenderer2_NewFrame();
    ImGui_ImplSDL2_NewFrame();
    ImGui::NewFrame();
#endif
}

void ImGui::RenderFrame () {
#ifndef DISABLE_DEAR_IMGUI
    ImGui::Render();
    ImGui_ImplSDLRenderer2_RenderDrawData(ImGui::GetDrawData());
#endif
}

void ImGui::ShutdownAll () {
#ifndef DISABLE_DEAR_IMGUI
    ImGui_ImplSDL2_Shutdown();
    DestroyContext();
#endif
}