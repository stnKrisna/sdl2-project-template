How to install ImGui:

1. Download DearImGui release
2. Extract the archive
3. Copy all .cpp & .h file in the root dir of the archive to this dir
4. Open the "backends" folder
5. Copy the following files to this dir:
    - imgui_impl_sdlrenderer2.cpp
    - imgui_impl_sdlrenderer2.h
    - imgui_impl_sdl2.cpp
    - imgui_impl_sdl2.h
6. Open "misc/cpp/" from the root and copy the following files to this dir:
    - imgui_stdlib.cpp
    - imgui_stdlib.h